package com.omgiapp.sat.activity;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.omgiapp.sat.Global;
import com.omgiapp.sat.R;
import com.omgiapp.sat.database.PerformanceDataSource;
import com.omgiapp.sat.model.GlossaryVocab;
import com.omgiapp.sat.model.Performance;

import java.util.List;

public class GlossaryActivity extends ActionBarActivity {

    private PerformanceDataSource performanceDataSource;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_glossary);

        recyclerView = (RecyclerView)findViewById(R.id.glossaryVocabs);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        performanceDataSource = new PerformanceDataSource(this);
        performanceDataSource.open();
        List<GlossaryVocab> glossaryVocabs = performanceDataSource.getGlossaryVocabs();

        adapter = new GlossaryPageAdapter(glossaryVocabs);
        recyclerView.setAdapter(adapter);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.action_bar);

        TextView actionBarTitle = (TextView)(actionBar.getCustomView().findViewById(R.id.action_bar_title));
        actionBarTitle.setText("Glossary (" + Global.numOfLearnedVocabs + ")");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_glossary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        performanceDataSource.close();
    }
}
