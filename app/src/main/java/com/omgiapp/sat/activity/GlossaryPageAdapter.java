package com.omgiapp.sat.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omgiapp.sat.R;
import com.omgiapp.sat.model.GlossaryVocab;

import java.util.List;

/**
 * Created by briankim on 1/7/16.
 */
public class GlossaryPageAdapter extends RecyclerView.Adapter<GlossaryPageAdapter.GlossaryVocabViewHolder>{

    private List<GlossaryVocab> glossaryVocabs;

    public GlossaryPageAdapter(List<GlossaryVocab> glossaryVocabs) {
        this.glossaryVocabs = glossaryVocabs;
    }

    public static class GlossaryVocabViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView glossaryVocabText;

        private RelativeLayout glossaryRow;

        GlossaryVocabViewHolder(View view) {
            super(view);

            glossaryVocabText = (TextView)view.findViewById(R.id.glossaryVocabText);
            glossaryRow = (RelativeLayout)view.findViewById(R.id.glossaryRow);

            glossaryRow.setOnClickListener(this);
        }

        public RelativeLayout getGlossaryRow() {
            return glossaryRow;
        }

        public TextView getGlossaryVocabText() {
            return glossaryVocabText;
        }

        @Override
        public void onClick(View view) {

            Context context = view.getContext();

            int vocabId = (int)view.getTag();

            Intent intent = new Intent(context, StudyActivity.class);
            intent.putExtra("vocabId", vocabId);
            context.startActivity(intent);
        }
    }

    @Override
    public GlossaryPageAdapter.GlossaryVocabViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.glossary_row, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new GlossaryVocabViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GlossaryVocabViewHolder viewHolder, int position) {
        GlossaryVocab glossaryVocab = glossaryVocabs.get(position);

        viewHolder.getGlossaryVocabText().setText(glossaryVocab.getVocab());
        viewHolder.getGlossaryRow().setTag(glossaryVocab.getVocabId());
    }

    @Override
    public int getItemCount() {
        return glossaryVocabs.size();
    }
}
