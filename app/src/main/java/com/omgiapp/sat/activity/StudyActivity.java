package com.omgiapp.sat.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.omgiapp.sat.R;
import com.omgiapp.sat.database.PerformanceDataSource;
import com.omgiapp.sat.database.VocabDataSource;
import com.omgiapp.sat.model.Vocab;

import java.util.List;

public class StudyActivity extends ActionBarActivity {

    VocabDataSource vocabDataSource;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study);

        openDatabaseConnections();

        Intent intent = getIntent();
        int vocabId = intent.getIntExtra("vocabId", 0);

        actionBar = getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.action_bar);

        populateView(vocabId);
    }

    private void openDatabaseConnections() {
        vocabDataSource = new VocabDataSource(this);
        vocabDataSource.open();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_study, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void populateView(int vocabId) {
        int requestSize = 5;

        String vocabWord = vocabDataSource.getVocab(vocabId);
        String definition = vocabDataSource.getDefinition(vocabId);
        List<String> synonyms = vocabDataSource.getSynonyms(vocabId, requestSize);
        List<String> examples = vocabDataSource.getExamples(vocabId, requestSize);

        TextView actionBarTitle = (TextView)(actionBar.getCustomView().findViewById(R.id.action_bar_title));
        actionBarTitle.setText("Studying " + vocabWord);

        TextView vocabText = (TextView)findViewById(R.id.vocabText);
        TextView definitionText = (TextView)findViewById(R.id.definitionText);
        TextView synonymsText = (TextView)findViewById(R.id.synonymsText);
        TextView examplesText = (TextView)findViewById(R.id.examplesText);

        vocabText.setText(vocabWord);
        definitionText.setText(definition);

        if (synonyms.size() == 0) {
            synonymsText.setVisibility(View.GONE);
        } else {
            synonymsText.setText(Html.fromHtml("<b>synonyms:</b><br>" + formatSynonyms(synonyms)));
        }

        if (examples.size() == 0) {
            examplesText.setVisibility(View.GONE);
        } else {
            examplesText.setText(Html.fromHtml("<b>examples:</b><br>" + formatExamples(examples, vocabWord)));
        }
    }

    private String formatSynonyms(List<String> synonyms) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i < synonyms.size(); i++) {
            if (i != 0) {
                stringBuilder.append(", ");
            }
            stringBuilder.append(synonyms.get(i));
        }
        return stringBuilder.toString();
    }

    private String formatExamples(List<String> examples, String vocabWord) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i=0; i < examples.size(); i++) {
            if (i != 0) {
                stringBuilder.append("<br><br>");
            }

            stringBuilder.append("\"");
            String[] exampleSentenceWords = examples.get(i).split(" ");

            for (int j=0; j < exampleSentenceWords.length; j++) {
                if (j != 0) {
                    stringBuilder.append(" ");
                }

                boolean match = true;

                if (exampleSentenceWords[j].length() < vocabWord.length() - 1) { // leave last char since tense may change ending
                    match = false;
                } else {
                    for (int k=0; k < vocabWord.length() - 1; k++) {
                        if (vocabWord.charAt(k) != exampleSentenceWords[j].charAt(k)) {
                            match = false;
                            break;
                        }
                    }
                }

                if (match) {
                    stringBuilder.append("<b>");
                    stringBuilder.append(exampleSentenceWords[j]);
                    stringBuilder.append("</b>");
                } else {
                    stringBuilder.append(exampleSentenceWords[j]);
                }
            }

            stringBuilder.append("\"");

        }
        return stringBuilder.toString();
    }

    public void returnToParent(View view) {
        finish();
    }
}
