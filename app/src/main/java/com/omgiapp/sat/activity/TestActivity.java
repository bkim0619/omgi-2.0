package com.omgiapp.sat.activity;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omgiapp.sat.Global;
import com.omgiapp.sat.R;
import com.omgiapp.sat.controller.Scheduler;
import com.omgiapp.sat.model.Vocab;

import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TestActivity extends ActionBarActivity {
    private Scheduler scheduler;

    // views
    TextView vocabType;
    TextView vocabText;
    TextView vocabMessage;

    List<Button> buttons = new ArrayList<Button>();
    Button synonymsButton;
    Button examplesButton;
    Button answerButton1;
    Button answerButton2;
    Button answerButton3;

    RelativeLayout incorrectView;

    ActionBar actionBar;

    Vocab currVocab;

    Random random = new Random();

    int correctAnswerIndex = -1;
    int lureIndex1 = -1;
    int lureIndex2 = -1;
    int lureIndex3 = -1;

    int shortDelay = 200;
    int longDelay = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        scheduler = new Scheduler(this);

        getViews();
        populateView();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        scheduler.close();
    }

    private void getViews() {
        vocabType = (TextView)findViewById(R.id.vocabType);
        vocabText = (TextView)findViewById(R.id.vocabText);
        vocabMessage = (TextView)findViewById(R.id.vocabMessage);

        synonymsButton = (Button)findViewById(R.id.synonymsButton);
        examplesButton = (Button)findViewById(R.id.examplesButton);
        answerButton1 = (Button)findViewById(R.id.answerButton1);
        answerButton2 = (Button)findViewById(R.id.answerButton2);
        answerButton3 = (Button)findViewById(R.id.answerButton3);

        buttons.add(synonymsButton);
        buttons.add(examplesButton);
        buttons.add(answerButton1);
        buttons.add(answerButton2);
        buttons.add(answerButton3);

        incorrectView = (RelativeLayout)findViewById(R.id.incorrectView);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true); // what shows the back button
        actionBar.setDisplayShowTitleEnabled(true); // what shows everything on the action bar
    }

    private void initializeButtonDecorations() {
        for (Button button : buttons) {
            button.setBackgroundResource(R.drawable.round_button_default);
        }
    }

    private void disableAllButtons() {
        for (Button button : buttons) {
            button.setClickable(false);
        }
    }

    private void enableAllButtons() {
        for (Button button : buttons) {
            button.setClickable(true);
        }
    }

    private void populateForNoneOfTheAbove() {
        answerButton3.setVisibility(View.INVISIBLE);

        correctAnswerIndex = random.nextInt(2);
        String lure = currVocab.getLures().get(lureIndex3);

        if (correctAnswerIndex == 0) {
            answerButton1.setText("(A) " + currVocab.getDefinition());
            answerButton2.setText("(B) " + lure);
        } else {
            answerButton1.setText("(A) " + lure);
            answerButton2.setText("(B) " + currVocab.getDefinition());
        }

        vocabMessage.setText("Pick the best definition: (A) or (B)");
        vocabMessage.setTextColor(getResources().getColor(R.color.light_charcoal));

        enableAllButtons();
    }


    public void provideFeedback(View view) {
        disableAllButtons();

        Button selectedButton = (Button)view;

        int selectedButtonIndex = Integer.parseInt(view.getTag().toString());

        boolean correct = selectedButtonIndex == correctAnswerIndex;

        if (correct) { // correct
            vocabMessage.setText("Correct");
            vocabMessage.setTextColor(getResources().getColor(R.color.pastel_green));

            selectedButton.setBackgroundResource(R.drawable.round_button_correct);

            // populate two additional choices
            if (selectedButtonIndex == Global.numOfChoices - 1) {
                Handler h = new Handler(Looper.getMainLooper());
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        populateForNoneOfTheAbove();
                    }
                };
                h.postDelayed(r, shortDelay);
                return;
            }
        } else { // incorrect
            vocabMessage.setText("Incorrect");
            vocabMessage.setTextColor(getResources().getColor(R.color.pastel_red));

            selectedButton.setBackgroundResource(R.drawable.round_button_incorrect);

            Button correctButton;
            if (correctAnswerIndex == 0) {
                correctButton = answerButton1;
            } else if (correctAnswerIndex == 1) {
                correctButton = answerButton2;
            } else {
                correctButton = answerButton3;
            }

            correctButton.setBackgroundResource(R.drawable.round_button_correct);

            if (correctAnswerIndex == Global.numOfChoices - 1) {
                correctButton.setText("(C) " + currVocab.getDefinition());
            }
        }

        // schedule this vocab for later
        scheduler.scheduleVocab(currVocab.getId(), correct);

        // transition to next vocab
        if (correct) {
            goToNextVocab(view);
        } else {
            showViewForIncorrect();
        }
    }

    private void showViewForIncorrect() {
        Handler h = new Handler(Looper.getMainLooper());
        Runnable r = new Runnable() {
            @Override
            public void run() {
                incorrectView.setVisibility(View.VISIBLE);
            }
        };
        h.postDelayed(r, shortDelay);

    }

    public void goToNextVocab(View view) {
        Handler h = new Handler(Looper.getMainLooper());
        Runnable r = new Runnable() {
            @Override
            public void run() {
                populateView();
            }
        };
        h.postDelayed(r, shortDelay);
    }

    public void studyVocab(View view) {
        Intent intent = new Intent(this, StudyActivity.class);
        intent.putExtra("vocabId", currVocab.getId());
        startActivity(intent);
    }

    public void populateView() {

        initializeButtonDecorations();

        incorrectView.setVisibility(View.INVISIBLE);

        answerButton3.setVisibility(View.VISIBLE);
        answerButton3.setText(getResources().getString(R.string.none_of_the_above));

        enableAllButtons();

        Vocab vocab = scheduler.getNextVocab();
        currVocab = vocab;

        actionBar.setTitle(String.format("%d of %d", vocab.isNewVocab()? Global.numOfLearnedVocabs + 1 : Global.numOfLearnedVocabs, Global.numOfVocabs));

        if (vocab.isNewVocab()) {
            vocabType.setText("New Vocab");
            vocabType.setTextColor(getResources().getColor(R.color.pastel_green));
        } else {
            vocabType.setText("Review Vocab");
            vocabType.setTextColor(getResources().getColor(R.color.pastel_orange));
        }

        vocabText.setText(vocab.getVocab());
        vocabMessage.setText("Pick the best definition");
        vocabMessage.setTextColor(getResources().getColor(R.color.light_charcoal));

        List<String> lures = vocab.getLures();

        // populate correct definition and lures
        correctAnswerIndex = random.nextInt(Global.numOfChoices);
        lureIndex1 = random.nextInt(lures.size());
        lureIndex2 = lureIndex1;
        while (lureIndex2 == lureIndex1) {
            lureIndex2 = random.nextInt(lures.size());
        }

        lureIndex3 = lureIndex2;
        while (lureIndex3 == lureIndex2 || lureIndex3 == lureIndex1) {
            lureIndex3 = random.nextInt(lures.size());
        }

        if (correctAnswerIndex != Global.numOfChoices - 1) {

            String lure = lures.get(lureIndex1);

            if (correctAnswerIndex == 0) {
                answerButton1.setText("(A) " + vocab.getDefinition());
                answerButton2.setText("(B) " + lure);
            } else if (correctAnswerIndex == 1) {
                answerButton1.setText("(A) " + lure);
                answerButton2.setText("(B) " + vocab.getDefinition());
            }
        } else {

            String lure1 = lures.get(lureIndex1);
            String lure2 = lures.get(lureIndex2);

            answerButton1.setText("(A) " + lure1);
            answerButton2.setText("(B) " + lure2);
        }
    }

    /*
    @Override
    public void onRestart()
    {
        super.onRestart();

        //Log.d(TestActivity.class.getName(), "restarting");

        populateView();
    }*/

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_test, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private enum Direction {
        UP,
        DOWN;
    }
}
