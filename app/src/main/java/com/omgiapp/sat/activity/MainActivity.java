package com.omgiapp.sat.activity;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omgiapp.sat.Global;
import com.omgiapp.sat.R;
import com.omgiapp.sat.database.PerformanceDataSource;
import com.omgiapp.sat.database.VocabDataSource;

public class MainActivity extends ActionBarActivity {

    PerformanceDataSource performanceDataSource;
    VocabDataSource vocabDataSource;

    TextView textView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openDatabaseConnections();
        getGlobals();

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar);

        initializeViews();

        populateView();
    }

    private void initializeViews() {

        textView = (TextView)findViewById(R.id.main_vocabs_learned);
        progressBar = (ProgressBar)findViewById(R.id.progress_bar);
    }

    private void populateView() {
        textView.setText(Global.numOfLearnedVocabs + "\nLearned");
        progressBar.setProgress(Global.numOfLearnedVocabs * 100 / Global.numOfVocabs);
    }

    private void openDatabaseConnections() {
        performanceDataSource = new PerformanceDataSource(this);
        performanceDataSource.open();

        vocabDataSource = new VocabDataSource(this);
        vocabDataSource.open();
    }

    private void getGlobals() {
        Global.numOfVocabs = vocabDataSource.getNumOfVocabs();
        Global.numOfLearnedVocabs = performanceDataSource.getNumOfLearnedVocabs();
    }

    public void startTest(View view) {
        Intent intent = new Intent(this, TestActivity.class);
        startActivity(intent);
    }

    public void goToGlossary(View view) {
        Intent intent = new Intent(this, GlossaryActivity.class);
        startActivity(intent);
    }

    public void goToAbout(View view) {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRestart()
    {
        super.onRestart();

        populateView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        performanceDataSource.close();
        vocabDataSource.close();
    }
}
