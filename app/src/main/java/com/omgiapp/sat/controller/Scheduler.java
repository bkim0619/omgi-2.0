package com.omgiapp.sat.controller;

import android.content.Context;
import android.util.Log;

import com.omgiapp.sat.Global;
import com.omgiapp.sat.database.PerformanceDataSource;
import com.omgiapp.sat.database.ScheduleDataSource;
import com.omgiapp.sat.database.VocabDataSource;
import com.omgiapp.sat.model.Performance;
import com.omgiapp.sat.model.Vocab;

import java.util.List;

/**
 * Created by briankim on 12/2/15.
 */
public class Scheduler {
    private ScheduleDataSource scheduleDataSource;
    private VocabDataSource vocabDataSource;
    private PerformanceDataSource performanceDataSource;

    private int[] schedule; // long array of learning schedule
    private int schedulePosition; // position in the schedule
    private boolean[] vocabsBitMap; // among vocabs, which has the user learned
    private int lastLearnedVocabId = -1;


    public Scheduler(Context context) {
        scheduleDataSource = new ScheduleDataSource(context);
        vocabDataSource = new VocabDataSource(context);
        performanceDataSource = new PerformanceDataSource(context);

        scheduleDataSource.open();
        vocabDataSource.open();
        performanceDataSource.open();

        schedule = scheduleDataSource.getSchedule();
        schedulePosition = scheduleDataSource.getSchedulePosition();
        vocabsBitMap = vocabDataSource.getLearnedVocabs();
        lastLearnedVocabId = performanceDataSource.getLastLearnedVocabId();
    }

    /**
     * Get next vocab in the queue
     *
     * @return
     */
    public Vocab getNextVocab() {
        int numOfLures = 100;

        int vocabId = schedule[schedulePosition % Global.numOfScheduleSlots];

        boolean newVocab = false;

        if (vocabId == -1) { // nothing scheduled, so get new vocab id
            vocabId = getNewVocabId();

            if (vocabId == -1) { // case where all vocabs have been learned

                do { // find next vocab to review
                    schedulePosition += 1;
                    vocabId = schedule[schedulePosition % Global.numOfScheduleSlots];
                } while (vocabId == -1);

                newVocab = false;

            } else {
                newVocab = true;
            }

        }

        String vocab = vocabDataSource.getVocab(vocabId);

        String definition = vocabDataSource.getDefinition(vocabId);

        // get lures
        List<Integer> lureIds = vocabDataSource.getLureIds(vocabId, numOfLures, newVocab);
        List<String> lures = vocabDataSource.getDefinitions(lureIds);

        return new Vocab(vocabId, newVocab, vocab, definition, lures);
    }

    /**
     * Get ceiling of log n (base 2)
     * @param n
     * @return
     */
    private int getLogBase2(int n) {

        if (n == 0) {
            return 0;
        }

        int originalN = n;

        int highestBit = 0;
        while (n > 1) {
            highestBit += 1;
            n >>= 1;
        }

        return originalN > (1 << highestBit)? highestBit + 1: highestBit;
    }

    /**
     * Get next new vocab id adjusting for difficulty level
     * @return
     */
    private int getNewVocabId() {
        int vocabId;
        if (Global.numOfLearnedVocabs <  getLogBase2(Global.numOfVocabs)) { // still in diagnostic mode
            int left = 0;
            int right = vocabsBitMap.length - 1;
            vocabId = (left + right) / 2;
            while (vocabsBitMap[vocabId] && (left != right)) {
                if (performanceDataSource.getLastCorrect(vocabId)) {
                    left = vocabId;
                } else {
                    right = vocabId;
                }

                vocabId = (left + right) / 2;
            }
        } else {
            vocabId = lastLearnedVocabId;
            int direction = performanceDataSource.getLastCorrect(vocabId) ? 1 : -1;

            while (vocabId < Global.numOfVocabs && vocabId >= 0 && vocabsBitMap[vocabId]) {
                vocabId += direction;
            }

            if (vocabId == Global.numOfVocabs || vocabId == -1) { // means this side is saturated
                vocabId = lastLearnedVocabId;
                while (vocabId < Global.numOfVocabs && vocabId >= 0 && vocabsBitMap[vocabId]) {
                    vocabId -= direction;
                }
            }

            if (vocabId == Global.numOfVocabs || vocabId == -1) { // means both sides are saturated
                vocabId = -1;
            }
        }

        return vocabId;
    }

    /**
     * Schedule vocab for later
     * @param vocabId
     * @param correct
     */
    public void scheduleVocab(int vocabId, boolean correct) {

        // insert performance
        Performance performance = new Performance(vocabId, 0, System.currentTimeMillis(), correct);
        performanceDataSource.insertPerformance(performance);

        // get all previous performances and determine next interval
        boolean[] correctSequence = performanceDataSource.getCorrectSequence(vocabId);

        int nextInterval = getNextInterval(correctSequence);

        // add and remove from schedule
        schedule[schedulePosition % Global.numOfScheduleSlots] = -1;
        scheduleDataSource.removeSchedule(schedulePosition);

        if (schedule[(schedulePosition + nextInterval) % Global.numOfScheduleSlots] != -1) {
            // find where the conflict ends
            int endIndex = schedulePosition + nextInterval;
            while (schedule[endIndex % Global.numOfScheduleSlots] != -1) {
                endIndex += 1;
            }

            // TODO: remove later
            /*
            for (int i=endIndex; i > schedulePosition + nextInterval; i--) {
                schedule[i % Global.numOfScheduleSlots] = schedule[(i-1) % Global.numOfScheduleSlots];

                if (i == endIndex) {
                    scheduleDataSource.insertSchedule(i, schedule[(i-1) % Global.numOfScheduleSlots]);
                } else {
                    scheduleDataSource.updateSchedule(i, schedule[(i-1) % Global.numOfScheduleSlots]);
                }
            }*/
            schedule[endIndex % Global.numOfScheduleSlots] = vocabId;
            scheduleDataSource.insertSchedule(endIndex, vocabId);
            //Log.d(Scheduler.class.getName(), vocabId + ":" + schedulePosition + ":" + endIndex);
        } else {
            schedule[(schedulePosition + nextInterval) % Global.numOfScheduleSlots] = vocabId;
            scheduleDataSource.insertSchedule(schedulePosition + nextInterval, vocabId);
            //Log.d(Scheduler.class.getName(), vocabId + ":" + schedulePosition + ":" + (schedulePosition + nextInterval));
        }

        schedulePosition += 1;
        scheduleDataSource.updateSchedule(-1, schedulePosition);

        vocabsBitMap[vocabId] = true;
        lastLearnedVocabId = performanceDataSource.getLastLearnedVocabId();

        Global.numOfLearnedVocabs = performanceDataSource.getNumOfLearnedVocabs();
    }

    /**
     * Get next interval for vocab, bump down when there are two consecutive incorrect answers
     * @param correctSequence
     * @return
     */
    private int getNextInterval(boolean[] correctSequence) {

        int[] intervals = new int[] {20, 100, 1000, 2 * Global.numOfVocabs};

        boolean failedPreviously = false;
        int intervalIndex = -1;
        for (int i=0; i < correctSequence.length; i++) {
            if (correctSequence[i]) {
                intervalIndex += 1;
                failedPreviously = false;
            } else {
                if (failedPreviously) {
                    intervalIndex -= 1;

                    if (intervalIndex < 0) {
                        intervalIndex = -1;
                    }

                    failedPreviously = false;
                } else {
                    failedPreviously = true;
                }
            }
        }

        if (intervalIndex < 0) {
            return intervals[0];
        } else if (intervalIndex >= intervals.length) {
            return intervals[intervals.length - 1];
        } else {
            return intervals[intervalIndex];
        }
    }


    public void close() {
        scheduleDataSource.close();
        vocabDataSource.close();
        performanceDataSource.close();
    }
}
