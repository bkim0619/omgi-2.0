package com.omgiapp.sat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.provider.ContactsContract;

import com.omgiapp.sat.model.GlossaryVocab;
import com.omgiapp.sat.model.Performance;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by briankim on 11/29/15.
 */
public class PerformanceDataSource {
    private DatabaseHelper databaseHelper;

    public PerformanceDataSource(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        databaseHelper.open();
    }

    public void close() {
        databaseHelper.close();
    }

    public int getLastLearnedVocabId() {
        // TODO: figure out how to do this in sql

        String query = String.format("SELECT %s, MAX(timestamp) FROM (SELECT %s, MIN(%s) as timestamp FROM %s GROUP BY %s);",
                                     DatabaseHelper.PERFORMANCES_COLUMN_ID,
                                     DatabaseHelper.PERFORMANCES_COLUMN_ID,
                                     DatabaseHelper.PERFORMANCES_COLUMN_TIMESTAMP,
                                     DatabaseHelper.PERFORMANCES_TABLE_NAME,
                                     DatabaseHelper.PERFORMANCES_COLUMN_ID);


        Cursor cursor = databaseHelper.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            int vocabId = cursor.getInt(0);
            cursor.close();
            return vocabId;
        } else {
            return -1;
        }
    }

    public List<GlossaryVocab> getGlossaryVocabs() {
        String query = String.format("SELECT DISTINCT a.%s, a.%s FROM %s a INNER JOIN %s b ON a.%s = b.%s ORDER BY a.%s;",
                                    DatabaseHelper.VOCABS_COLUMN_ID,
                                    DatabaseHelper.VOCABS_COLUMN_VOCAB,
                                    DatabaseHelper.VOCABS_TABLE_NAME,
                                    DatabaseHelper.PERFORMANCES_TABLE_NAME,
                                    DatabaseHelper.VOCABS_COLUMN_ID,
                                    DatabaseHelper.PERFORMANCES_COLUMN_ID,
                                    DatabaseHelper.VOCABS_COLUMN_VOCAB);

        Cursor cursor = databaseHelper.rawQuery(query, null);

        List<GlossaryVocab> glossaryVocabs = new ArrayList<GlossaryVocab>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            glossaryVocabs.add(new GlossaryVocab(cursor.getInt(0), cursor.getString(1)));
            cursor.moveToNext();
        }

        cursor.close();

        return glossaryVocabs;
    }

    public boolean getLastCorrect(int vocabId) {
        String tableName = DatabaseHelper.PERFORMANCES_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.PERFORMANCES_COLUMN_CORRECT};
        String whereClause = DatabaseHelper.PERFORMANCES_COLUMN_ID + " = ?";
        String[] whereArgs = {Integer.toString(vocabId)};
        String orderBy = DatabaseHelper.PERFORMANCES_COLUMN_TIMESTAMP + " DESC";

        Cursor cursor = databaseHelper.query(false, tableName, tableColumns, whereClause, whereArgs, null, null, orderBy, null);

        cursor.moveToFirst();
        boolean correct = cursor.getInt(0) == 1;
        cursor.close();

        return correct;
    }

    /**
     * Insert performance statistics for the given vocab
     *
     * @param performance performance object
     */
    public void insertPerformance(Performance performance) {
        int vocabId = performance.getVocabId();

        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.PERFORMANCES_COLUMN_ID, vocabId);
        values.put(DatabaseHelper.PERFORMANCES_COLUMN_DEFINITION_ID, performance.getDefinitionId());
        values.put(DatabaseHelper.PERFORMANCES_COLUMN_TIMESTAMP, performance.getTimestamp().getTime());
        values.put(DatabaseHelper.PERFORMANCES_COLUMN_CORRECT, performance.getCorrect()? 1 : 0);

        databaseHelper.insert(DatabaseHelper.PERFORMANCES_TABLE_NAME, null, values);
    }

    /**
     * Get sequence of correct/incorrect for the vocab
     * @param vocabId
     * @return
     */
    public boolean[] getCorrectSequence(int vocabId) {
        String[] columns = {DatabaseHelper.PERFORMANCES_COLUMN_CORRECT};
        String[] whereArgs = {Integer.toString(vocabId)};

        Cursor cursor = databaseHelper.query(false,
                                             DatabaseHelper.PERFORMANCES_TABLE_NAME,
                                             columns,
                                             DatabaseHelper.PERFORMANCES_COLUMN_ID + " = ?",
                                             whereArgs,
                                             null,
                                             null,
                                             DatabaseHelper.PERFORMANCES_COLUMN_SEQUENCE_ID,
                                             null);

        boolean[] toReturn = new boolean[cursor.getCount()];
        cursor.moveToFirst();
        int cnt = 0;
        while (!cursor.isAfterLast()) {
            toReturn[cnt++] = cursor.getInt(0) == 1;
            cursor.moveToNext();
        }

        cursor.close();
        return toReturn;
    }


    /**
     * Get number of vocabs learned by user
     *
     * @return Number of vocabs learned by the user
     */
    public int getNumOfLearnedVocabs() {

        String[] columns = {DatabaseHelper.PERFORMANCES_COLUMN_ID};

        Cursor cursor = databaseHelper.query(true,
                                            DatabaseHelper.PERFORMANCES_TABLE_NAME,
                                            columns,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null);

        int numOfLearnedVocabs = cursor.getCount();
        cursor.close();
        return numOfLearnedVocabs;
    }
}
