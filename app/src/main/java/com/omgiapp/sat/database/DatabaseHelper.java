package com.omgiapp.sat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.omgiapp.sat.Global;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by briankim on 11/29/15.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    // database info
    private static final String DATABASE_NAME = "omgi.db";
    private static final int DATABASE_VERSION = 2;

    private final String DATABASE_PATH; // location to put database file
    private final Context context;
    private SQLiteDatabase database;

    // vocabulary table
    public static final String VOCABS_TABLE_NAME = "vocabs";
    public static final String VOCABS_COLUMN_ID = "_id";
    public static final String VOCABS_COLUMN_VOCAB = "vocab";

    // definition table
    public static final String DEFINITIONS_TABLE_NAME = "definitions";
    public static final String DEFINITIONS_COLUMN_ID = "_id";
    public static final String DEFINITIONS_COLUMN_DEFINITION_ID = "definition_id";
    public static final String DEFINITIONS_COLUMN_DEFINITION = "definition";
    public static final String DEFINITIONS_COLUMN_TYPE = "type";

    // user performance table
    public static final String PERFORMANCES_TABLE_NAME = "performances";
    public static final String PERFORMANCES_COLUMN_SEQUENCE_ID = "sequence_id";
    public static final String PERFORMANCES_COLUMN_ID = "_id";
    public static final String PERFORMANCES_COLUMN_DEFINITION_ID = "definition_id";
    public static final String PERFORMANCES_COLUMN_TIMESTAMP = "timestamp";
    public static final String PERFORMANCES_COLUMN_CORRECT = "correct";

    // example sentences table
    public static final String EXAMPLES_TABLE_NAME = "examples";
    public static final String EXAMPLES_COLUMN_ID = "_id";
    public static final String EXAMPLES_COLUMN_DEFINITION_ID = "definition_id";
    public static final String EXAMPLES_COLUMN_EXAMPLE_ID = "example_id";
    public static final String EXAMPLES_COLUMN_EXAMPLE = "example";

    // synonyms table
    public static final String SYNONYMS_TABLE_NAME = "synonyms";
    public static final String SYNONYMS_COLUMN_ID = "_id";
    public static final String SYNONYMS_COLUMN_DEFINITION_ID = "definition_id";
    public static final String SYNONYMS_COLUMN_SYNONYM_ID = "synonym_id";
    public static final String SYNONYMS_COLUMN_SYNONYM = "synonym";

    // schedule table
    public static final String SCHEDULE_TABLE_NAME = "schedule";
    public static final String SCHEDULE_COLUMN_POSITION = "position";
    public static final String SCHEDULE_COLUMN_ID = "_id";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.context = context;
        DATABASE_PATH = context.getFilesDir().getAbsolutePath() + "/databases/";
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Log.d(DatabaseHelper.class.getName(), "onCreate");
        // decided to not update original database file, always use onUpgrade trail
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Log.d(DatabaseHelper.class.getName(), "onUpgrade");

        switch(oldVersion) {
            case 0:
            case 1:
                List<String> vocabs = new ArrayList<String>();
                vocabs.add("scad");
                vocabs.add("poseur");
                vocabs.add("perquisite");
                vocabs.add("satiate");
                vocabs.add("unfrock");
                vocabs.add("centigrade");

                List<String> definitions = new ArrayList<String>();
                definitions.add("A great quantity");
                definitions.add("Person who pretends to be sophisticated, elegant, etc., to impress others");
                definitions.add("Any gain above stipulated salary");
                definitions.add("Satisfy fully");
                definitions.add("To strip a priest or minister of church authority");
                definitions.add("Measure of temperature used widely in Europe");

                updateDefinitions(db, vocabs, definitions);

                // update examples
                vocabs = new ArrayList<String>();
                vocabs.add("scad");

                List<String> scadExamples = new ArrayList<String>();
                scadExamples.add("Refusing Dave's offer to lend him a shirt, Phil replied, 'No, thanks, I've got scads of clothes.'");

                List<List<String>> examples = new ArrayList<List<String>>();
                examples.add(scadExamples);

                updateExamples(db, vocabs, examples);

                //Log.d(DatabaseHelper.class.getName(), "case 1 upgrade");
                // we want both updates, so no break statement here
        }

        db.setVersion(DATABASE_VERSION);
    }

    /**
     * Get vocab id for a vocab
     * @param vocab
     * @return
     */
    private int getVocabId(SQLiteDatabase db, String vocab) {
        String[] columns = {VOCABS_COLUMN_ID};
        String selection = VOCABS_COLUMN_VOCAB + " = ?";
        String[] selectionArgs = {vocab};

        Cursor cursor = db.query(true, VOCABS_TABLE_NAME, columns, selection, selectionArgs, null, null, null, null);

        cursor.moveToFirst();

        int vocabId = cursor.getInt(0);

        cursor.close();

        return vocabId;
    }

    /**
     * Batch update vocab definition
     *
     * @param db
     * @param vocabs
     * @param definitions
     */
    private void updateDefinitions(SQLiteDatabase db, List<String> vocabs, List<String> definitions) {
        for (int i=0; i < vocabs.size(); i++) {
            String vocab = vocabs.get(i);
            int vocabId = getVocabId(db, vocab);

            ContentValues values = new ContentValues();
            values.put(DEFINITIONS_COLUMN_DEFINITION, definitions.get(i));
            String whereClause = DEFINITIONS_COLUMN_ID + " = ?";
            String[] whereArgs = {Integer.toString(vocabId)};
            db.update(DEFINITIONS_TABLE_NAME, values, whereClause, whereArgs);
        }
    }

    /**
     * Batch update vocab examples
     *
     * @param db
     * @param vocabs
     * @param examples
     */
    private void updateExamples(SQLiteDatabase db, List<String> vocabs, List<List<String>> examples) {
        for (int i=0; i < vocabs.size(); i++) {
            String vocab = vocabs.get(i);
            int vocabId = getVocabId(db, vocab);

            // delete all examples first
            String whereClause = EXAMPLES_COLUMN_ID + " = ?";
            String[] whereArgs = {Integer.toString(vocabId)};
            db.delete(EXAMPLES_TABLE_NAME, whereClause, whereArgs);

            for (int j=0; j < examples.size(); j++) {
                ContentValues values = new ContentValues();
                values.put(EXAMPLES_COLUMN_ID, vocabId);
                values.put(EXAMPLES_COLUMN_DEFINITION_ID, 0);
                values.put(EXAMPLES_COLUMN_EXAMPLE_ID, j);
                values.put(EXAMPLES_COLUMN_EXAMPLE, examples.get(i).get(j));

                db.insert(EXAMPLES_TABLE_NAME, null, values);
            }
       }
    }

    @Override
    public synchronized void close() {
        if (database != null) {
            database.close();
        }
        super.close();
    }

    private boolean checkDatabase() {
        //File databaseFile = context.getDatabasePath(DATABASE_NAME);
        //return databaseFile.exists();

        File databaseFile = new File(DATABASE_PATH + DATABASE_NAME);
        return databaseFile.exists();
    }

    private void copyDatabaseFromFile() throws IOException {
        //Log.d(DatabaseHelper.class.getName(), "copying database");

        // open database file in the assets folder as input stream
        InputStream input = context.getAssets().open(DATABASE_NAME);

        // create intermediate directories if not present
        File f = new File(DATABASE_PATH);
        boolean createDirectory = f.mkdirs();

        //Log.d(MySQLiteHelper.class.getName(), createDirectory? "created directory" : "didn't create directory");

        // open the empty db as an output stream
        OutputStream output = new FileOutputStream(DATABASE_PATH + DATABASE_NAME);

        // transfer bytes from input file to output file
        byte[] buffer = new byte[1024];
        int length;
        while ((length = input.read(buffer)) > 0) {
            output.write(buffer, 0, length);
        }

        // close the input/output streams
        output.flush();
        output.close();
        input.close();
    }

    public void open() throws SQLException {
        boolean databasePresent = checkDatabase();

        // TODO: use singleton for database helper
        //Log.d(DatabaseHelper.class.getName(), databasePresent? "database present" : "database not present");

        if (!databasePresent) {
            try {
                copyDatabaseFromFile();
            } catch (IOException e) {
                throw new SQLException("Error copying database");
            }
        }

        database = SQLiteDatabase.openDatabase(DATABASE_PATH + DATABASE_NAME, null, SQLiteDatabase.OPEN_READWRITE);

        if (!databasePresent) {
            database.setVersion(1); // semantically should be 1, but is 0 when copied from assets folder
        }

        //Log.d(DatabaseHelper.class.getName(), database.getVersion() + "");
        if (database.getVersion() < DATABASE_VERSION) {
            onUpgrade(database, database.getVersion(), DATABASE_VERSION);
        }
    }

    public Cursor query(boolean distinct, String table, String[] columns,
                        String selection, String[] selectionArgs, String groupBy,
                        String having, String orderBy, String limit) {
        return database.query(distinct, table, columns, selection, selectionArgs, groupBy, having, orderBy, limit);
    }

    public long insert(String table, String nullColumnHack, ContentValues values) {
        return database.insert(table, nullColumnHack, values);
    }

    public int delete(String table, String whereClause, String[] whereArgs) {
        return database.delete(table, whereClause, whereArgs);
    }

    public int update(String table, ContentValues values, String whereClause, String[] whereArgs) {
        return database.update(table, values, whereClause, whereArgs);
    }

    public Cursor rawQuery(String query, String[] selectionArgs) {
        return database.rawQuery(query, selectionArgs);
    }
}
