package com.omgiapp.sat.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.omgiapp.sat.Global;


/**
 * Created by briankim on 12/2/15.
 */
public class ScheduleDataSource {
    private DatabaseHelper databaseHelper;

    public ScheduleDataSource(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public void open() {
        databaseHelper.open();
    }

    public void close() {
        databaseHelper.close();
    }

    public int getSchedulePosition() {
        String tableName = DatabaseHelper.SCHEDULE_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.SCHEDULE_COLUMN_ID};
        String whereClause = DatabaseHelper.SCHEDULE_COLUMN_POSITION + " = -1";

        Cursor cursor = databaseHelper.query(true, tableName, tableColumns, whereClause, null, null, null, null, null);

        cursor.moveToFirst();
        int position = cursor.getInt(0);

        cursor.close();

        return position;
    }

    /**
     * Remove schedule specified by position
     * Note: vocabId does not need to be specified
     * @param position
     */
    public void removeSchedule(int position) {
        databaseHelper.delete(DatabaseHelper.SCHEDULE_TABLE_NAME, DatabaseHelper.SCHEDULE_COLUMN_POSITION + "=" + position, null);
    }

    public void insertSchedule(int position, int vocabId) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.SCHEDULE_COLUMN_POSITION, position);
        values.put(DatabaseHelper.SCHEDULE_COLUMN_ID, vocabId);

        databaseHelper.insert(DatabaseHelper.SCHEDULE_TABLE_NAME, null, values);
    }

    public void updateSchedule(int position, int vocabId) {
        ContentValues values = new ContentValues();
        values.put(DatabaseHelper.SCHEDULE_COLUMN_ID, vocabId);

        databaseHelper.update(DatabaseHelper.SCHEDULE_TABLE_NAME, values, DatabaseHelper.SCHEDULE_COLUMN_POSITION + "=" + position, null);
    }

    public int[] getSchedule() {
        int[] schedule = new int[Global.numOfScheduleSlots];
        for (int i=0; i < schedule.length; i++) {
            schedule[i] = -1;
        }

        String tableName = DatabaseHelper.SCHEDULE_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.SCHEDULE_COLUMN_POSITION, DatabaseHelper.SCHEDULE_COLUMN_ID};
        String whereClause = DatabaseHelper.SCHEDULE_COLUMN_POSITION + " >= 0";
        String orderBy = DatabaseHelper.SCHEDULE_COLUMN_POSITION;

        Cursor cursor = databaseHelper.query(false, tableName, tableColumns, whereClause, null, null, null, orderBy, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            int position = cursor.getInt(0) % Global.numOfScheduleSlots;
            int vocabId = cursor.getInt(1);
            schedule[position] = vocabId;
            cursor.moveToNext();
        }

        cursor.close();

        return schedule;
    }
}
