package com.omgiapp.sat.database;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import com.omgiapp.sat.Global;
import com.omgiapp.sat.model.Vocab;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by briankim on 12/2/15.
 */
public class VocabDataSource {
    private DatabaseHelper databaseHelper;

    public VocabDataSource(Context context) {
        databaseHelper = new DatabaseHelper(context);
    }

    public void open() {
        databaseHelper.open();
    }

    public void close() {
        databaseHelper.close();
    }

    /**
     * Get the vocabulary word for the given vocab id
     * @param vocabId
     * @return
     */
    public String getVocab(int vocabId) {
        String tableName = DatabaseHelper.VOCABS_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.VOCABS_COLUMN_VOCAB};
        String whereClause = String.format("%s = ?", DatabaseHelper.VOCABS_COLUMN_ID);
        String[] whereArgs = {Integer.toString(vocabId)};
        Cursor cursor = databaseHelper.query(false, tableName, tableColumns, whereClause, whereArgs, null, null, null, null);
        cursor.moveToFirst();
        String vocab = cursor.getString(0);
        cursor.close();
        return vocab;
    }

    /**
     * Get lure ids
     * @param vocabId vocab for which lures are needed
     * @param requestSize size of the lures, not guaranteed
     * @param newVocab whether this vocab is new or review
     * @return
     */
    public List<Integer> getLureIds(int vocabId, int requestSize, boolean newVocab) {
        List<Integer> toReturn = new ArrayList<Integer>();

        if (newVocab) {
            int cnt = 0;
            int lureId = vocabId + 1;

            // sweep up
            while (lureId < Global.numOfVocabs && cnt < requestSize) {
                toReturn.add(lureId);
                cnt += 1;
                lureId += 1;
            }

            lureId = vocabId - 1;

            // sweep down
            while (lureId >= 0 && cnt < requestSize) {
                toReturn.add(lureId);
                cnt += 1;
                lureId -= 1;
            }

        } else {

            // TODO: think about this logic again (where should the lures come from?)
            String tableName = DatabaseHelper.PERFORMANCES_TABLE_NAME;
            String[] tableColumns = {DatabaseHelper.PERFORMANCES_COLUMN_ID};
            String whereClause = String.format("%s <> ?", DatabaseHelper.PERFORMANCES_COLUMN_ID);
            String[] whereArgs = {Integer.toString(vocabId)};
            String orderBy = DatabaseHelper.PERFORMANCES_COLUMN_SEQUENCE_ID + " DESC";
            String limit = Integer.toString(requestSize);

            Cursor cursor = databaseHelper.query(true, tableName, tableColumns, whereClause, whereArgs, null, null, orderBy, limit);
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                toReturn.add(cursor.getInt(0));
                cursor.moveToNext();
            }

            cursor.close();
        }

        return toReturn;
    }

    /**
     * Get definitions for a vocab
     * @param vocabId
     * @return
     */
    public String getDefinition(int vocabId) {
        String tableName = DatabaseHelper.DEFINITIONS_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.DEFINITIONS_COLUMN_DEFINITION};
        String whereClause = String.format("%s = ? and %s = 0", DatabaseHelper.DEFINITIONS_COLUMN_ID, databaseHelper.DEFINITIONS_COLUMN_DEFINITION_ID);
        String[] whereArgs = {Integer.toString(vocabId)};
        Cursor cursor = databaseHelper.query(false, tableName, tableColumns, whereClause, whereArgs, null, null, null, null);
        cursor.moveToFirst();
        String definition = cursor.getString(0);
        cursor.close();
        return definition;
    }

    public List<String> getDefinitions(List<Integer> vocabIds) {
        String tableName = DatabaseHelper.DEFINITIONS_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.DEFINITIONS_COLUMN_DEFINITION};

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("(");
        for (int i=0; i < vocabIds.size(); i++) {
            if (i != 0) {
                stringBuilder.append(",");
            }
            stringBuilder.append(vocabIds.get(i));
        }
        stringBuilder.append(")");
        String values = stringBuilder.toString();

        String whereClause = String.format("%s IN %s and %s = 0", DatabaseHelper.DEFINITIONS_COLUMN_ID, values, databaseHelper.DEFINITIONS_COLUMN_DEFINITION_ID);
        Cursor cursor = databaseHelper.query(false, tableName, tableColumns, whereClause, null, null, null, null, null);
        cursor.moveToFirst();

        List<String> definitions = new ArrayList<String>();
        while (!cursor.isAfterLast()) {
            definitions.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return definitions;
    }

    public List<String> getSynonyms(int vocabId, int requestSize) {
        String tableName = DatabaseHelper.SYNONYMS_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.SYNONYMS_COLUMN_SYNONYM};
        String whereClause = String.format("%s = ? and %s = 0", DatabaseHelper.SYNONYMS_COLUMN_ID, DatabaseHelper.SYNONYMS_COLUMN_DEFINITION_ID);
        String[] whereArgs = {Integer.toString(vocabId)};
        String orderBy = DatabaseHelper.SYNONYMS_COLUMN_SYNONYM_ID;
        String limit = Integer.toString(requestSize);

        Cursor cursor = databaseHelper.query(false, tableName, tableColumns, whereClause, whereArgs, null, null, orderBy, limit);
        cursor.moveToFirst();

        List<String> synonyms = new ArrayList<String>();
        while (!cursor.isAfterLast()) {
            synonyms.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return synonyms;
    }

    public List<String> getExamples(int vocabId, int requestSize) {
        String tableName = DatabaseHelper.EXAMPLES_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.EXAMPLES_COLUMN_EXAMPLE};
        String whereClause = String.format("%s = ?", DatabaseHelper.EXAMPLES_COLUMN_ID);
        String[] whereArgs = {Integer.toString(vocabId)};
        String orderBy = DatabaseHelper.EXAMPLES_COLUMN_EXAMPLE_ID;
        String limit = Integer.toString(requestSize);

        Cursor cursor = databaseHelper.query(false, tableName, tableColumns, whereClause, whereArgs, null, null, orderBy, limit);
        cursor.moveToFirst();

        List<String> examples = new ArrayList<String>();
        while (!cursor.isAfterLast()) {
            examples.add(cursor.getString(0));
            cursor.moveToNext();
        }

        cursor.close();
        return examples;
    }


    /**
     * Get a bitmap of learned vocabs
     * @return
     */
    public boolean[] getLearnedVocabs() {
        boolean[] learnedVocabs = new boolean[Global.numOfVocabs];

        int[] learnedVocabIds = getLearnedVocabIds();

        for (int i=0; i < learnedVocabIds.length; i++) {
            int vocabId = learnedVocabIds[i];
            learnedVocabs[vocabId] = true;
        }

        return learnedVocabs;
    }

    /**
     * Get total number of vocabs
     * @return
     */
    public int getNumOfVocabs() {
        String tableName = DatabaseHelper.VOCABS_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.VOCABS_COLUMN_ID};
        Cursor cursor = databaseHelper.query(false, tableName, tableColumns, null, null, null, null, null, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    private int[] getLearnedVocabIds() {
        String tableName = DatabaseHelper.PERFORMANCES_TABLE_NAME;
        String[] tableColumns = {DatabaseHelper.PERFORMANCES_COLUMN_ID};

        Cursor cursor = databaseHelper.query(true, tableName, tableColumns, null, null, null, null, null, null);
        cursor.moveToFirst();

        int[] vocabIds = new int[cursor.getCount()];
        int index = 0;
        while (!cursor.isAfterLast()) {
            vocabIds[index++] = cursor.getInt(0);
            cursor.moveToNext();
        }

        cursor.close();
        return vocabIds;
    }
}
