package com.omgiapp.sat;

/**
 * Created by briankim on 12/3/15.
 */
public class Global {
    public static int numOfVocabs = 3351;
    public static int numOfLearnedVocabs = -1;
    public static int numOfChoices = 3;
    public static int numOfScheduleSlots = 1048576; // 2^20
}
