package com.omgiapp.sat.model;

import java.util.List;

/**
 * Created by briankim on 12/2/15.
 */
public class Vocab { // used for TestActivity
    private int id;
    private boolean isNewVocab;

    public String getVocab() {
        return vocab;
    }

    private String vocab;
    private String definition;
    private List<String> lures;

    public Vocab(int id, boolean isNewVocab, String vocab, String definition, List<String> lures) {
        this.id = id;
        this.isNewVocab = isNewVocab;
        this.vocab = vocab;
        this.definition = definition;
        this.lures = lures;
    }

    public int getId() {
        return id;
    }

    public boolean isNewVocab() {
        return isNewVocab;
    }

    public String getDefinition() {
        return definition;
    }

    public List<String> getLures() {
        return lures;
    }
}
