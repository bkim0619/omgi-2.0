package com.omgiapp.sat.model;

/**
 * Created by briankim on 1/7/16.
 */
public class GlossaryVocab {

    private int vocabId;
    private String vocab;

    public GlossaryVocab(int vocabId, String vocab) {
        this.vocabId = vocabId;
        this.vocab = vocab;
    }

    public int getVocabId() {
        return vocabId;
    }

    public String getVocab() {
        return vocab;
    }
}
