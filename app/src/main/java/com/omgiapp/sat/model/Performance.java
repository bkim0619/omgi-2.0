package com.omgiapp.sat.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by briankim on 11/29/15.
 */
public class Performance {

    private int id;
    private int definitionId;
    private Date timestamp;
    private boolean correct;

    public Performance (int id, int definitionId, long timestamp, boolean correct) {
        this.id = id;
        this.definitionId = definitionId;
        this.timestamp = new Date(timestamp);
        this.correct = correct;
    }

    public int getVocabId() {
        return id;
    }

    public int getDefinitionId() {
        return definitionId;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public boolean getCorrect() {
        return correct;
    }
}
